# LINEARIZER

C Library implementing automatic linearisation algorithm from our
our paper "Linearising Discrete Time Hybrid Systems" submitted to
IEEE Transactions on Automatic Control.

**Input**

- f : nonlinear function to be linearised
- D : bounded closed region (f must be lipschitz continuous on this region)
- lambda : function that given a bounded closed region estimates the lipschitz constant for function f on that interval
- eps : linerization error threshold
- m : number of sampling points within one interval

**Output**

- I : linear cover defined on D
- F- : linear family of functions underappoximating function f
- F+ : linear family of functions overapproximating function f

## Building prerequisites

LINEARIZER library depends on the following external libraries:

* [GNU Scientific Library](http://www.gnu.org/software/gsl)
* [LPSOLVER](https://bitbucket.org/mclab/lpsolver)

## Building

This library building system is using CMake.

```
mkdir build
cd build
cmake ..
make
```

You can also generate documentation via [doxygen](http://doxygen.org).

```
make doc
```
