/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of LINEARIZER open source library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COVER_H_
#define COVER_H_

typedef struct Cover Cover;

extern Cover *
lin_cover_new(size_t /*num*/, size_t /*arity*/);

extern Cover *
lin_cover_new_from_file(FILE * /*stream*/);

extern void
lin_cover_destroy(Cover ** /*cover_ptr*/);

extern void
lin_cover_fprintf(FILE * /*stream*/, Cover const * /*cover*/);

extern size_t
lin_cover_get_arity(Cover const * /*cover*/);

extern size_t
lin_cover_get_num(Cover const * /*cover*/);

extern gsl_vector_const_view
lin_cover_get_interval_lb(Cover const * /*cover*/, size_t /*i*/);

extern void
lin_cover_set_interval_lb(Cover * /*cover*/, size_t /*i*/, size_t /*j*/, double /*value*/);

extern gsl_vector_const_view
lin_cover_get_interval_ub(Cover const * /*cover*/, size_t /*i*/);

extern void
lin_cover_set_interval_ub(Cover * /*cover*/, size_t /*i*/, size_t /*j*/, double /*value*/);

#endif /* #ifndef COVER_H_ */
