/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of LINEARIZER open source library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINEARIZATION_H_
#define LINEARIZATION_H_

typedef struct Linearization Linearization;

typedef void (*LinCallback)(gsl_vector const *, gsl_vector const *);

extern Linearization *
linearization_new(FuncPtr /*f*/, size_t /*arity*/, LipschitzFuncPtr /*lipschitz_f*/, gsl_vector const * /*a*/, gsl_vector const * /*b*/, double /*eps*/, size_t /*m*/, LPSolver * /*solver*/, LinCallback /*cb*/);

extern Linearization *
linearization_new_from_file(FILE * /*stream*/);

extern void
linearization_destroy(Linearization ** /*lin_ptr*/);

extern void
linearization_dump(FILE * /*stream*/, Linearization const * /*lin*/);

#endif /* #ifndef LINEARIZATION_H_ */
