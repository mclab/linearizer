/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of LINEARIZER open source library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>

#include <gsl/gsl_matrix.h>

#include "memory.h"
#include "cover.h"

struct Cover {
  size_t arity;
  size_t num;
  gsl_matrix *lbs;
  gsl_matrix *ubs;
};

Cover *
lin_cover_new(size_t num, size_t arity) {
  Cover *cover;
  MALLOC(cover, 1, Cover);
  cover->num = num;
  cover->arity = arity;
  cover->lbs = gsl_matrix_alloc(num, arity);
  cover->ubs = gsl_matrix_alloc(num, arity);
  return cover;
}

Cover *
lin_cover_new_from_file(FILE *stream) {
  Cover *cover = NULL;
  size_t num;
  size_t arity;
  if (fscanf(stream, "%zu %zu", &num, &arity) != 2) {
    goto exit;
  }
  gsl_matrix *lbs = gsl_matrix_alloc(num, arity);
  if (gsl_matrix_fscanf(stream, lbs) != 0) {
    gsl_matrix_free(lbs);
    goto exit;
  }
  gsl_matrix *ubs = gsl_matrix_alloc(num, arity);
  if (gsl_matrix_fscanf(stream, ubs) != 0) {
    gsl_matrix_free(lbs);
    gsl_matrix_free(ubs);
    goto exit;
  }
  MALLOC(cover, 1, Cover);
  cover->num = num;
  cover->arity = arity;
  cover->lbs = lbs;
  cover->ubs = ubs;
exit:
  return cover;
}

void
lin_cover_destroy(Cover **cover_ptr) {
  Cover *cover = *cover_ptr;
  gsl_matrix_free(cover->lbs);
  gsl_matrix_free(cover->ubs);
  FREE(*cover_ptr);
}

void
lin_cover_fprintf(FILE *stream, Cover const *cover) {
  fprintf(stream, "%zu %zu\n", cover->num, cover->arity);
  gsl_matrix_fprintf(stream, cover->lbs, "%g");
  gsl_matrix_fprintf(stream, cover->ubs, "%g");
}

size_t
lin_cover_get_arity(Cover const *cover) {
  return cover->arity;
}

size_t
lin_cover_get_num(Cover const *cover) {
  return cover->num;
}

gsl_vector_const_view
lin_cover_get_interval_lb(Cover const *cover, size_t i) {
  return gsl_matrix_const_row(cover->lbs, i);
}

void
lin_cover_set_interval_lb(Cover *cover, size_t i, size_t j, double value) {
  gsl_matrix_set(cover->lbs, i, j, value);
}

gsl_vector_const_view
lin_cover_get_interval_ub(Cover const *cover, size_t i) {
  return gsl_matrix_const_row(cover->ubs, i);
}

void
lin_cover_set_interval_ub(Cover *cover, size_t i, size_t j, double value) {
  gsl_matrix_set(cover->ubs, i, j, value);
}
