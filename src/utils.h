/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of the LINEARIZER library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H_
#define UTILS_H_

extern int
lin_vsprintf_smart(char ** /*str*/, char const * /*fmt*/, va_list /*args*/);

extern int
lin_sprintf_smart(char ** /*str*/, char const * /*fmt*/, ...);

#endif /* #ifndef UTILS_H_ */
