/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of LINEARIZER open source library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include "memory.h"
#include "func.h"
#include "sample.h"

struct Sample {
  size_t arity;
  size_t num;
  gsl_matrix *xs;
  gsl_vector *ys;
  double distance;
};

Sample *
lin_sample_new(FuncPtr f, size_t arity, gsl_vector const *a, gsl_vector const *b, size_t m) {
  size_t i, j, k;
  Sample *sample;
  MALLOC(sample, 1, Sample);
  sample->arity = arity;
  sample->num = (size_t) pow(m, arity);
  sample->xs = gsl_matrix_alloc(sample->num, sample->arity + 1);
  sample->ys = gsl_vector_alloc(sample->num);
  double *delta;
  MALLOC(delta, arity, double);
  sample->distance = 0;
  for (j = 0; j < arity; ++j) {
    delta[j] = (gsl_vector_get(b, j) - gsl_vector_get(a, j)) / (m - 1);
    sample->distance += delta[j] * delta[j];
  }
  sample->distance = sqrt(sample->distance);
  for (i = 0; i < sample->num - 1; ++i) {
    gsl_matrix_set(sample->xs, i, 0, 1);
    for (j = 0; j < sample->arity; ++j) {
      k = ((i / (size_t) pow(m, j)) % m);
      gsl_matrix_set(sample->xs, i, j + 1, gsl_vector_get(a, j) + delta[j] * k);
    }
  }
  gsl_matrix_set(sample->xs, i, 0, 1);
  for (j = 0; j < arity; ++j) {
    gsl_matrix_set(sample->xs, i, j + 1, gsl_vector_get(b, j));
  }
  double *x;
  MALLOC(x, arity, double);
  for (i = 0; i < sample->num; ++i) {
    for (j = 0; j < arity; ++j) {
      x[j] = gsl_matrix_get(sample->xs, i, j + 1);
    }
    gsl_vector_set(sample->ys, i, f(x));
  }
  FREE(x);
  FREE(delta);
  return sample;
}

void
lin_sample_destroy(Sample **sample_ptr) {
  Sample *sample = *sample_ptr;
  gsl_matrix_free(sample->xs);
  gsl_vector_free(sample->ys);
  FREE(*sample_ptr);
}

size_t
lin_sample_get_arity(Sample const *sample) {
  return sample->arity;
}

size_t
lin_sample_get_num(Sample const *sample) {
  return sample->num;
}

gsl_vector_const_view
lin_sample_get_x(Sample const *sample, size_t i) {
  return gsl_matrix_const_row(sample->xs, i);
}

double
lin_sample_get_y(Sample const *sample, size_t i) {
  return gsl_vector_get(sample->ys, i);
}

double
lin_sample_get_distance(Sample const *sample) {
  return sample->distance;
}

extern void
lin_sample_print(FILE *s, Sample const *sample) {
  for (size_t i = 0; i < sample->num; ++i) {
    fprintf(s, "( ");
    for (size_t j = 1; j < sample->arity + 1; ++j) {
      fprintf(s, "%g ", gsl_matrix_get(sample->xs, i, j));
    }
    fprintf(s, ")\t%g\n", gsl_vector_get(sample->ys, i));
  }
}
