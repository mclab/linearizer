/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of LINEARIZER open source library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAMPLE_H_
#define SAMPLE_H_

typedef struct Sample Sample;

extern Sample *
lin_sample_new(FuncPtr /*f*/, size_t /*arity*/, gsl_vector const * /*a*/, gsl_vector const * /*b*/, size_t /*m*/);

extern void
lin_sample_destroy(Sample ** /*sample_ptr*/);

extern size_t
lin_sample_get_arity(Sample const * /*sample*/);

extern size_t
lin_sample_get_num(Sample const * /*sample*/);

extern gsl_vector_const_view
lin_sample_get_x(Sample const * /*sample*/, size_t /*i*/);

extern double
lin_sample_get_y(Sample const * /*sample*/, size_t /*i*/);

extern double
lin_sample_get_distance(Sample const * /*sample*/);

extern void
lin_sample_print(FILE * /*s*/, Sample const * /*sample*/);

#endif /* #ifndef SAMPLE_H_ */
