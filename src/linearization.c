/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of the LINEARIZER library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>

#include <lpsolver.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include "memory.h"
#include "func.h"
#include "cover.h"
#include "sample.h"
#include "linearization.h"

struct Linearization {
  Cover *cover;
  gsl_matrix *f_plus;
  gsl_matrix *f_minus;
};

static LPSolverProblem *
generate_milp(Sample const * /*sample*/, double /*lipschitz_constant*/, LPSolver *solver);

static Cover *
split(gsl_vector const * /*a*/, gsl_vector const * /*b*/, size_t /*arity*/);


static Linearization *
aux(FuncPtr f, size_t arity, LipschitzFuncPtr lipschitz_f, gsl_vector const *a, gsl_vector const *b, double eps, size_t m, size_t depth, LPSolver *solver, LinCallback cb) {
  size_t i, j, k, l;
  Linearization *res;
  MALLOC(res, 1, Linearization);
  Sample *sample = lin_sample_new(f, arity, a, b, m);
  /* generate milp */
  double lipschitz_constant = lipschitz_f(a, b);
  LPSolverProblem *problem = generate_milp(sample, lipschitz_constant, solver);
  lin_sample_destroy(&sample);
  /* solve milp */
  int status = lpsolver_problem_optimize(problem);
  assert(status != 0); // LP problem must always have solution
  double error_lp = lpsolver_problem_get_obj_value(problem);
  /* if error is less than eps */
  if (error_lp <= eps) {
    res->cover = lin_cover_new(1, arity);
    for (j = 0; j < arity; ++j) {
      lin_cover_set_interval_lb(res->cover, 0, j, gsl_vector_get(a, j));
      lin_cover_set_interval_ub(res->cover, 0, j, gsl_vector_get(b, j));
    }
    /* set f_plus and f_minus from milp result */
    res->f_plus = gsl_matrix_alloc(1, arity + 1);
    res->f_minus = gsl_matrix_alloc(1, arity + 1);
    for (j = 0; j < arity + 1; ++j) {
      gsl_matrix_set(res->f_minus, 0, j, lpsolver_problem_get_col_value(problem, (int) j));
      gsl_matrix_set(res->f_plus, 0, j, lpsolver_problem_get_col_value(problem, (int) (arity + 1 + j)));
    }
    lpsolver_problem_destroy(&problem);
    if (cb != NULL) {
      cb(a, b);
    }
  }
  else {
    lpsolver_problem_destroy(&problem);
    size_t num = 0;
    Cover *cover = split(a, b, arity);
    Linearization **res_part;
    MALLOC(res_part, lin_cover_get_num(cover), Linearization *);
    for (i = 0; i < lin_cover_get_num(cover); ++i) {
      gsl_vector_const_view lb = lin_cover_get_interval_lb(cover, i);
      gsl_vector_const_view ub = lin_cover_get_interval_ub(cover, i);
      res_part[i] = aux(f, arity, lipschitz_f, &lb.vector, &ub.vector, eps, m, depth + 1, solver, cb);
      num += lin_cover_get_num(res_part[i]->cover);
    }
    /* merge res_part into res */
    res->cover = lin_cover_new(num, arity);
    res->f_plus = gsl_matrix_alloc(num, arity + 1);
    res->f_minus = gsl_matrix_alloc(num, arity + 1);
    for (i = 0, k = 0; i < lin_cover_get_num(cover); ++i) {
      for (l = 0; l < lin_cover_get_num(res_part[i]->cover); ++l) {
        gsl_vector_const_view f_plus_row = gsl_matrix_const_row(res_part[i]->f_plus, l);
        gsl_matrix_set_row(res->f_plus, k, &f_plus_row.vector);
        gsl_vector_const_view f_minus_row = gsl_matrix_const_row(res_part[i]->f_minus, l);
        gsl_matrix_set_row(res->f_minus, k, &f_minus_row.vector);
        gsl_vector_const_view lb = lin_cover_get_interval_lb(res_part[i]->cover, l);
        gsl_vector_const_view ub = lin_cover_get_interval_ub(res_part[i]->cover, l);
        for (j = 0; j < arity; ++j) {
          lin_cover_set_interval_lb(res->cover, k, j, gsl_vector_get(&lb.vector, j));
          lin_cover_set_interval_ub(res->cover, k, j, gsl_vector_get(&ub.vector, j));
        }
        ++k;
      }
      linearization_destroy(&res_part[i]);
    }
    FREE(res_part);
    lin_cover_destroy(&cover);
  }
  return res;
}

Linearization *
linearization_new(FuncPtr f, size_t arity, LipschitzFuncPtr lipschitz_f, gsl_vector const *a, gsl_vector const *b, double eps, size_t m, LPSolver *solver, LinCallback cb) {
  return aux(f, arity, lipschitz_f, a, b, eps, m, 0, solver, cb);
}

Linearization *
linearization_new_from_file(FILE *stream) {
  Linearization *res = NULL;
  Cover *cover = lin_cover_new_from_file(stream);
  if (cover == NULL) {
    goto exit;
  }
  size_t num = lin_cover_get_num(cover);
  size_t arity = lin_cover_get_arity(cover);
  gsl_matrix *f_minus = gsl_matrix_alloc(num, arity + 1);
  if (gsl_matrix_fscanf(stream, f_minus) != 0) {
    lin_cover_destroy(&cover);
    gsl_matrix_free(f_minus);
    goto exit;
  }
  gsl_matrix *f_plus = gsl_matrix_alloc(num, arity + 1);
  if (gsl_matrix_fscanf(stream, f_plus) != 0) {
    lin_cover_destroy(&cover);
    gsl_matrix_free(f_minus);
    gsl_matrix_free(f_plus);
    goto exit;
  }
  MALLOC(res, 1, Linearization);
  res->cover = cover;
  res->f_minus = f_minus;
  res->f_plus = f_plus;
exit:
  return res;
}

void
linearization_destroy(Linearization **lin_ptr) {
  Linearization *lin = *lin_ptr;
  lin_cover_destroy(&lin->cover);
  gsl_matrix_free(lin->f_plus);
  gsl_matrix_free(lin->f_minus);
  FREE(*lin_ptr);
}

void
linearization_fprintf(FILE *stream, Linearization const * lin) {
  lin_cover_fprintf(stream, lin->cover);
  gsl_matrix_fprintf(stream, lin->f_minus, "%g");
  gsl_matrix_fprintf(stream, lin->f_plus, "%g");
}

static LPSolverProblem *
generate_milp(Sample const *sample, double lipschitz_constant, LPSolver *solver) {
  size_t i, j, k;
  double shift = lipschitz_constant * lin_sample_get_distance(sample);
  size_t p = lin_sample_get_arity(sample) + 1;
  size_t num_rows = lin_sample_get_num(sample) * 3;
  /* p for under, p for over, 1 for error */
  size_t num_cols = 2 * p + 1;
  LPSolverProblem *problem = lpsolver_problem_new(solver, "lin", (int) num_rows, (int) num_cols);
  for (j = 0; j < 2 * p + 1; ++j) {
    lpsolver_problem_set_col_bounds(problem, (int) j, LPSOLVER_COL_BOUNDS_TYPE_FR, 0, 0);
  }
  for (i = 0; i < lin_sample_get_num(sample); ++i) {
    gsl_vector_const_view x = lin_sample_get_x(sample, i);
    /* under */
    for (j = 0; j < p; ++j) {
      lpsolver_problem_set_row_coeff(problem, (int) (3 * i), (int) j, gsl_vector_get(&x.vector, j));
      lpsolver_problem_set_row_coeff(problem, (int) (3 * i), (int) (p + j), 0);
    }
    lpsolver_problem_set_row_coeff(problem, (int) (3 * i), (int) (2 * p), 0);
    lpsolver_problem_set_row_bounds(problem, (int) (3 * i), LPSOLVER_ROW_BOUNDS_TYPE_UB, 0, lin_sample_get_y(sample, i) - shift);
    /* over */
    for (j = 0; j < p; ++j) {
      lpsolver_problem_set_row_coeff(problem, (int) (3 * i + 1), (int) j, 0);
      lpsolver_problem_set_row_coeff(problem, (int) (3 * i + 1), (int) (p + j), gsl_vector_get(&x.vector, j));
    }
    lpsolver_problem_set_row_coeff(problem, (int) (3 * i + 1), (int) (2 * p), 0);
    lpsolver_problem_set_row_bounds(problem, (int) (3 * i + 1), LPSOLVER_ROW_BOUNDS_TYPE_LB, lin_sample_get_y(sample, i) + shift, 0);
    /* error */
    for (j = 0; j < p; ++j) {
      lpsolver_problem_set_row_coeff(problem, (int) (3 * i + 2), (int) (p + j), gsl_vector_get(&x.vector, j));
      lpsolver_problem_set_row_coeff(problem, (int) (3 * i + 2), (int) j, -gsl_vector_get(&x.vector, j));
    }
    lpsolver_problem_set_row_coeff(problem, (int) (3 * i + 2), (int) (2 * p), -1);
    lpsolver_problem_set_row_bounds(problem, (int) (3 * i + 2), LPSOLVER_ROW_BOUNDS_TYPE_UB, 0, 0);
  }
  lpsolver_problem_set_obj_dir(problem, LPSOLVER_OBJ_DIR_MIN);
  for (j = 0; j < 2 * p - 1; ++j) {
    lpsolver_problem_set_obj_coeff(problem, (int) j, 0);
  }
  lpsolver_problem_set_obj_coeff(problem, (int) (2 * p), 1);
  return problem;
}

static Cover *
split(gsl_vector const *a, gsl_vector const *b, size_t arity) {
  size_t i, j, k;
  size_t n = (size_t) pow(2, arity);
  Cover *cover = lin_cover_new(n, arity);
  double *delta;
  MALLOC(delta, arity, double);
  for (j = 0; j < arity; ++j) {
    delta[j] = (gsl_vector_get(b, j) - gsl_vector_get(a, j)) / 2;
  }
  for (i = 0; i < n; ++i) {
    for (j = 0; j < arity; ++j) {
      k = ((i / (size_t) pow(2, j)) % 2);
      lin_cover_set_interval_lb(cover, i, j, gsl_vector_get(a, j) + delta[j] * k);
      lin_cover_set_interval_ub(cover, i, j, gsl_vector_get(a, j) + delta[j] * (k + 1));
    }
  }
  FREE(delta);
  return cover;
}
