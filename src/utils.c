/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of the LINEARIZER library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "memory.h"
#include "utils.h"

int
lin_vsprintf_smart(char **str, char const *fmt, va_list args) {
  *str = NULL;
  va_list args_copy;
  va_copy(args_copy, args);
  int count = vsnprintf(NULL, 0, fmt, args_copy);
  va_end(args_copy);
  va_copy(args_copy, args);
  if (count >= 0) {
    char *buffer;
    MALLOC(buffer, (size_t) (count + 1), char);
    count = vsnprintf(buffer, count + 1, fmt, args_copy);
    if (count < 0) {
      FREE(buffer);
      return count;
    }
    *str = buffer;
  }
  va_end(args_copy);
  return count;
}

int
lin_sprintf_smart(char **str, char const *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  int count = lin_vsprintf_smart(str, fmt, args);
  va_end(args);
  return count;
}
