/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of the LINEARIZER library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#define MALLOC(var, count, type)          \
do {                                      \
  (var) = malloc((count) * sizeof(type)); \
  if ((var) == NULL) {                    \
    abort();                              \
  }                                       \
} while(0)

#define CALLOC(var, count, type)          \
do {                                      \
  (var) = calloc((count) * sizeof(type)); \
  if ((var) == NULL) {                    \
    abort();                              \
  }                                       \
} while(0)

#define REALLOC(var, var_old, count, size)            \
do {                                                  \
  (var) = realloc((var_old), (count) * (size)); \
  if ((var) == NULL) {                                \
    abort();                                          \
  }                                                   \
} while(0)

#define FREE(var) \
do {              \
  free((var));    \
  (var) = NULL;   \
} while (0)

#endif /* #ifndef MEMORY_H_ */
