/*
 * Copyright (C) 2015 Vadim Alimguzhin
 *
 * This file is part of LINEARIZER open source library.
 *
 * LINEARIZER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LINEARIZER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file linearizer.h
 * 
 * Public interface of LINEARIZER library.
 */

#ifndef LINEARIZER_H_
#define LINEARIZER_H_

/**
 * \typedef FuncPtr
 *
 * Pointer to the function that takes array of reals as input and returns a real.
 * Is used as a type for nonlinear functions to be linearized.
 */
typedef double (*FuncPtr)(double *);

/**
 * \typedef LipschitzFuncPtr
 *
 * Pointer to the function that takes bounded closed region as interval and returns a real.
 * Is used as a type for the functions estimating lipschitz constant.
 */
typedef double (*LipschitzFuncPtr)(gsl_vector const *, gsl_vector const *);

/**
 * \typedef LinCallback
 *
 * Pointer to the callback function, that will be called by linearization
 * process each time when linearization for an interval is found.
 * Can be used for example to print out the progress.
 * It is feeded with the interval bounds.
 */
typedef void (*LinCallback)(gsl_vector const *, gsl_vector const *);

/**
 * \typedef Cover
 *
 * Linear cover.
 */
typedef struct Cover Cover;

/**
 * \typedef Linearization
 *
 * Shortcut for struct Linearization.
 */
typedef struct Linearization Linearization;

/**
 * \struct Linearization
 *
 * Object of this struct represents linearization, that is linear cover and
 * corresponding linear piecewise over- and under- approximations.
 */
struct Linearization {
  Cover *cover; /**< linear cover */
  gsl_matrix *f_plus; /**< i-th row of this matrix defines linear overapproximation for i-th interval of the cover */
  gsl_matrix *f_minus; /**< i-th row of this matrix defines linear underapproximation for i-th interval of the cover */
};

/**
 * Getter of cover arity.
 *
 * \param cover linear cover
 *
 * \return cover arity
 * */
extern size_t
lin_cover_get_arity(Cover const *cover);

/**
 * Getter of number of intervals.
 *
 * \param cover linear cover
 *
 * \return number of intervals
 * */
extern size_t
lin_cover_get_num(Cover const *cover);

/**
 * Getter of lower bound.
 *
 * \param cover linear cover
 * \param i interval index 
 *
 * \return lower bound of interval i
 * */
extern gsl_vector_const_view
lin_cover_get_interval_lb(Cover const *cover, size_t i);

/**
 * Getter of upper bound.
 * 
 * \param cover linear cover
 * \param i interval index 
 *
 * \return upper bound of interval i
 * */
extern gsl_vector_const_view
lin_cover_get_interval_ub(Cover const *cover, size_t i);

/**
 * The main function of LINEARIZER library.
 *
 * \param f pointer to the function to be linearized
 * \param arity function arity
 * \param lipschitz_f pointer to the function that given an interval estimates lipschitz constant of function f on that interval
 * \param a lower bound of the interval
 * \param b upper bound of the interval
 * \param eps  linearization error threshold
 * \param m number of sampling points (more sampling points -> slower computation, but smaller number of intervals)
 * \param solver LP solver instance
 * \param cb callback function (can be NULL)
 *
 * \return linearization of function f on the hyperinterval [a,b], such that its error <= eps
 */
extern Linearization *
linearization_new(FuncPtr f, size_t arity, LipschitzFuncPtr lipschitz_f, gsl_vector *a, gsl_vector *b, double eps, size_t m, LPSolver *solver, LinCallback cb);

/**
 * Read linearization from file.
 *
 * \param input stream
 *
 * \return linearization object if read performed successfully, NULL otherwise
 */
extern Linearization *
linearization_new_from_file(FILE *stream);

/**
 * Destructor for linearization object.
 *
 * \param linearization_ptr pointer to linearization object
 */
extern void
linearization_destroy(Linearization **linearization_ptr);

/**
 * Print linearization to stream.
 *
 * \param lin linearization
 * \param stream output stream
 */
extern void
linearization_fprintf(FILE *stream, Linearization const *lin);

#endif /* #ifndef LINEARIZER_H_ */
